package tianti;

import java.util.Scanner;

public class Pta9 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// Is PAT&TAP symmetric?
		String str = sc.nextLine();
		System.out.println(MaxSubStr(str) + "=" + MaxSubStr(str).length());

	}

	// 判断对称的方法
	public static boolean symmetric(String str) {
		// 利用char数组进行遍历 从尾巴和头部同时进行比较取出对称最大的部分
		char[] c_arr = str.toCharArray();
		int min = 0;
		int max = c_arr.length - 1;
		while (min < max) {
			if (c_arr[min++] != c_arr[max--]){
				return false;
			}
		}
		return true;
	}
	
	// 返回最长对称数组
	public static String MaxSubStr(String str) {
		String substr = "";
		for (int i = 0; i < str.length(); i++) {
			for (int j = str.length(); j >= i; j--) {
				String temp = str.substring(i, j);
				if (symmetric(temp) && temp.length() >= substr.length())
					substr = temp;
			}
		}
		return substr;
	}
}