package tianti;

import java.util.*;

public class Main {
	
	static String str="";
	
	class Node {
		int data;
		Node leftChild;
		Node rightChild;

		public Node(int idata) {
			data = idata;
		}
	}

	// 根节点
	private Node root;

	// 接受中序遍历数组和后序遍历数组，进入递归方法
	Node reCreatTree(int[] mid, int[] last) {
		root = reCreate(mid, 0, mid.length - 1, last, 0, last.length - 1);
		return root;
	}

	// 参数依次为中序遍历数组，起始点，结束点；后序遍历数组，起始点，结束点
	Node reCreate(int[] mid, int midStrat, int midEnd, int[] last, int lastStart, int lastEnd) {
		// 防止前后交叉导致已选过的数值重复选中
		if (lastStart > lastEnd) {
			return null;
		}
		// 防止数组越界
		if (lastStart < 0 || lastEnd < 0 || midStrat < 0 || midEnd < 0) {
			return null;
		}
		// 给所选节点赋值
		Node newNode = new Node(last[lastEnd]);
		int index = IndexOfRootInMid(mid, newNode.data);
		// 所选节点左子树的数量
		int leftNum = index - midStrat;
		// 向左子树往下走
		newNode.leftChild = reCreate(mid, midStrat, index - 1, last, lastStart, lastStart + leftNum - 1);
		// 向右子树往下走
		newNode.rightChild = reCreate(mid, index + 1, midEnd, last, lastStart + leftNum, lastEnd - 1);
		// 返回节点
		return newNode;
	}

	// 获得所选子树的根节点在中序数组中的下标
	int IndexOfRootInMid(int[] mid, int rootData) {
		int index = -1;
		for (int i = 0; i < mid.length - 1; i++) {
			if (mid[i] == rootData) {
				index = i;
				break;
			}
		}
		return index;
	}

	// 输出树
	public void displayTree() {
		Stack<Node> globalStack = new Stack<Node>();
		globalStack.push(root);
		int nBlanks = 32;
		boolean isRowEmpty = false;
		while (isRowEmpty == false) {
			Stack<Node> localStack = new Stack<Node>();
			isRowEmpty = true;

			while (globalStack.isEmpty() == false) {
				Node temp = (Node) globalStack.pop();
				if (temp != null) {
//						System.out.print(temp.data+" ");
						str+=temp.data;
						localStack.push(temp.leftChild);
						localStack.push(temp.rightChild);
						if (temp.leftChild != null || temp.rightChild != null)
							isRowEmpty = false;
				} else {
					localStack.push(null);
					localStack.push(null);
				}
				
			}
			nBlanks /= 2;
			while (localStack.isEmpty() == false)
				globalStack.push(localStack.pop());
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
//		System.out.println("请输入N：");
		int N = sc.nextInt();
		int[] preOrder = new int[N];
		int[] inOrder = new int[N];
		int[] postOrder = new int[N];
//		System.out.println("请输入后序遍历：");
		for (int i = 0; i < N; i++) {
			postOrder[i] = sc.nextInt();
		}
//		System.out.println("请输入中序遍历：");
		for (int i = 0; i < N; i++) {
			inOrder[i] = sc.nextInt();
		}
		Main tree = new Main();
		tree.reCreatTree(inOrder, postOrder);
		tree.displayTree();
//		System.out.println(str);
		for(int i=0;i<str.length();i++){
			if(i==str.length()-1){
				System.out.print(str.substring(i,i+1));
				break;
			}
			System.out.print(str.substring(i,i+1)+" ");
		}
	}
}