package tianti;

import java.util.Scanner;

public class Pta1 {

	static int gcd(int a, int b) {
		if (b == 0)
			return a;
		else
			return gcd(b, a % b);
	}

	static int lcm(int a, int b) {
		return a * b / gcd(a, b);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int a, b, ta = 0, tb = 0;
		for (int i = 1; i <= n; i++) {
			String str = sc.next();
			a = Integer.parseInt(str.split("/")[0]);
			b = Integer.parseInt(str.split("/")[1]);
			if (i == 1) {
				ta = a;
				tb = b;
			} else {
				int sb;
				sb = lcm(b, tb);// 求出最小公倍数
				// 两个分数合并
				ta = sb / tb * ta + sb / b * a;
				tb = sb;
			}
			// 分数化简
			int g = gcd(Math.abs(ta), Math.abs(tb));
			ta = ta / g;
			tb = tb / g;
		}
		if (ta == 0 || Math.abs(ta) >= tb) {
			System.out.print(ta / tb + " ");
			if (ta % tb == 0) {
				System.out.println();
			} else {
				System.out.println("" + (Math.abs(ta) % tb) + "/" + tb + "");
			}
		} else {
			System.out.println("" + ta + "/" + tb + "");
		}
	}

}
