package tianti;

import java.util.Scanner;

public class Pta7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// 测试集
		// 350322111122334455
		Scanner sc = new Scanner(System.in);
		String[] Z = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
		char[] M = new char[] { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' };
		//权重
		int[] pow = new int[] { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };

		int N = sc.nextInt();
		char[][] id_spilt = new char[N][18];
		int allpassed = 1;
		// 将N个身份证号码存入二维表
		for (int i = 0; i < N; i++) {
			String id_card = sc.next();
			for (int j = 0; j < 18; j++) {
				id_spilt[i][j] = Character.valueOf(id_card.charAt(j));
			}
		}
		// 进行判断
		for (int i = 0; i < N; i++) {
			int z = 0;
			for (int j = 0; j < 17; j++) {
				z += ((id_spilt[i][j]) - '0') * pow[j];
			}
			//取模
			z %= 11;
			if (M[z] != id_spilt[i][17]) {
				allpassed = 0;
				for (int k = 0; k < 18; k++) {
					System.out.print(id_spilt[i][k]);
				}
				System.out.println();
			}
		}
		if (allpassed == 1) {
			System.out.println("All passed");
		}
	}

}
