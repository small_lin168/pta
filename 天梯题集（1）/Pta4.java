package tianti;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		System.out.println(fib(N));
	}

	public static int fib(int N) {
		int temp = 1;
		int sum = 0;
		for (int i = 1; i <= N; i++) {
			if (N == 0) {
				sum = 1;
			} else {
				temp *= i;
				sum += temp;
			}
		}
		return sum;
	}

}
