import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		while (true) {
			String str = sc.next();
			int num = Integer.parseInt(str);
			if (num >= 10000 && num <= 20000) {
				System.out.print(Integer.toBinaryString(num)+",");
				System.out.print(Integer.toOctalString(num)+",");
				System.out.println(Integer.toHexString(num));
			}
			else{
				int sum=0;
				int temp=1;
				num=Math.abs(num);
				str=Integer.toString(num);
				for(int i=0;i<str.length();i++){
						temp=Integer.parseInt(String.valueOf(str.charAt(i)));
						System.out.print(temp+" ");
						sum+=temp;
					}
					System.out.println(sum);
				}
		}

	}

}
