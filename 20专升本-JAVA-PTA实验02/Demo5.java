import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final double epsilon=0.0001;
		final double step = 0.0001;
		Scanner sc =new Scanner(System.in);
		while(true){
			double x,y;
			x=sc.nextDouble();
			y=0;
			if(x<0){
				System.out.println("NaN");
			}else{
				while(Math.pow(y,2)<x && Math.abs(x-(Math.pow(y, 2))) > epsilon){
					y+=step;
				}
				System.out.printf("%.6f\n", y);
			}
			
		}
	}

}
