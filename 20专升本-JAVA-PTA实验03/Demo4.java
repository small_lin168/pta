import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
			int n=Integer.valueOf(sc.nextLine());
			String[] id_card_arr=new String[n];//存储用户输入的原身份证号码
			String[] id_card_temp=new String[n];//用来遍历转换做操作
			int[] id_card_sort=new int[n];//用来进行Arrays.sort做准备
			
			//将输入的多个身份证存入
			for(int i =0;i<n;i++)	{
				String id_card=sc.nextLine();
				id_card_arr[i]=id_card;
			}
			//将数组存到id_card_temp 这样不会对原数组进行破坏
			for(int i=0;i<id_card_temp.length;i++){
				id_card_temp[i]=id_card_arr[i];
			}
			while(true){
			String method=sc.nextLine();
			switch (method) {
			case "sort1":
				//遍历数组进行字符串截取6-14位
				for(int i=0;i<id_card_arr.length;i++){
					id_card_temp[i]=id_card_arr[i].substring(6,14);
				}
				//进行排序
				Arrays.sort(id_card_temp);
				//利用字符串特效进行字符分割
				for(int i=0;i<id_card_temp.length;i++){
					System.out.println(""+id_card_temp[i].substring(0,4)+"-"
							+ ""+id_card_temp[i].substring(4,6)+"-"
									+ ""+id_card_temp[i].substring(6,8)+"");
				}
				break;
				
			case "sort2":
				String[] no_repeat=new String[n];
				//将数组存到id_card_temp 这样不会对原数组进行破坏
				for(int i=0;i<id_card_temp.length;i++){
					id_card_temp[i]=id_card_arr[i];
				}
				//遍历数组进行字符串截取6-14位
				for(int i=0;i<id_card_temp.length;i++){
					String a=id_card_temp[i].substring(6,14);
					id_card_sort[i]=Integer.parseInt(a);
				}
				//排序
				Arrays.sort(id_card_sort);
				//利用排序去遍历id_card_temp 再利用equal字符串比较的方法来获取完整身份证
				for(int i=0;i<id_card_sort.length;i++){
					id_card_temp[i]=String.valueOf(id_card_sort[i]);
					for(int j=0;j<id_card_arr.length;j++){
						if(id_card_temp[i].equals(id_card_arr[j].substring(6,14))){
							no_repeat[i]=id_card_arr[j];
						}
					}
				}
				for(int k=0;k<no_repeat.length;k++){
					System.out.println(no_repeat[k]);
				}
				break;
				
			default:
				System.out.println("exit");
				break;
			}
		}
	}

}
