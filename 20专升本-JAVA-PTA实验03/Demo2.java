import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String method;
		String num;
		int[] result = new int[1];
		while (true) {
			method = sc.nextLine();
			switch (method) {
			// 打印fib函数效果
			case "fib":
				int result1 = Integer.valueOf(sc.nextLine());
				for (int i = 1; i <= result1; i++) {
					if(i==result1){
						System.out.println(fib(i));
						break;
					}
					System.out.print(fib(i) + " ");
				}
				break;
			// 打印sort函数效果
			case "sort":
				String result2;
				result2 = sc.nextLine();
				String[] result2_1 = result2.split(" ");
				int[] result2_2 = new int[result2_1.length];
				for (int i = 0; i < result2_1.length; i++) {
					result2_2[i] = Integer.parseInt(result2_1[i]);
				}
				result = result2_2;
				Arrays.sort(result2_2);
				System.out.println(Arrays.toString(result2_2));
				break;
			case "search":
				int temp = Integer.valueOf(sc.nextLine());
				int flag = 0;
				for (int i = 0; i < result.length; i++) {
					if (result[i] == temp) {
						flag = 1;
						System.out.println(i);
						break;
					}
				}
				if (flag == 0) {
					System.out.println(-1);
				}
				break;
				
			case "getBirthDate":
				int count = Integer.valueOf(sc.nextLine());
				while(count>0){
					// 输入身份证号
					String id_card = sc.nextLine();
					// 定义年月日
					String year, month, day;
					// 利用string字符串截取
					year = id_card.substring(6, 10);
					month = id_card.substring(10, 12);
					day = id_card.substring(12, 14);
					System.out.println("" + year + "-" + month + "-" + day + "");
					count--;
				}
				break;
				
			default:
				System.out.println("exit");
				break;
			}

		}
	}

	// 打印斐波那契数列
	public static int fib(int num) {
		if (num == 0 || num == 1) {
			return num;
		} else {
			return fib(num - 2) + fib(num - 1);
		}
	}

}
