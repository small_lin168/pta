package pta5;

import java.util.Scanner;

public class Demo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		String str = sc.next();
		while(true){
			if(str.length()<80){
				StringBuffer sb = new StringBuffer(str);
				//将字符反转
				sb.reverse();
				int count = 0;
				//每位对比
				for (int i = 0; i < str.length(); i++) {
					if (str.charAt(i) == sb.charAt(i)) {
						count++;
					}
				}
				//相同的位数与长度做对比
				if (str.length() == count) {			
					System.out.println("yes");
				} else {
					System.out.println("no");
				}
				break;
			}else{
				System.out.println("请重新输入小于80个字符的字符串：");
				str = sc.next();
			}
		}
	}

}
