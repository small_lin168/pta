## PTA作业代码
### 作业目录
- 实验01
        - [7-1 古埃及探秘-金字塔](./20专升本-JAVA-PTA实验01/Demo1.java)
- 实验02
	- [7-1 古埃及探秘-金字塔](./20专升本-JAVA-PTA实验02/Demo1.java)
	- [7-2 jmu-Java-01入门-第一个PAT上Java程序](./20专升本-JAVA-PTA实验02/Demo2.java)
	- [7-3 jmu-Java-01入门-取数字](./20专升本-JAVA-PTA实验02/Demo3.java)
	- [7-4 jmu-Java-01入门-取数字浮点数](./20专升本-JAVA-PTA实验02/Demo4.java)
	- [7-5 jmu-Java-01入门-开根号](./20专升本-JAVA-PTA实验02/Demo5.java)
- 实验03
	- [7-1 校园竞赛-十位评委](./20专升本-JAVA-PTA实验03/Demo1.java)
	- [7-2 jmu-Java-02基本语法-01-综合小测验](./20专升本-JAVA-PTA实验03/Demo2.java)
	- [7-3 jmu-Java-02基本语法-02-StringBuilder](./20专升本-JAVA-PTA实验03/Demo3.java)
	- [7-4 jmu-Java-02基本语法-03-身份证排序](./20专升本-JAVA-PTA实验03/Demo4.java)
- 实验04
	- [7-1 jmu-Java-03面向对象基础-01-构造函数与toString](./20专升本-JAVA-PTA实验04/Demo1.java)
	- [7-2 jmu-Java-03面向对象基础-02-构造函数与初始化块](./20专升本-JAVA-PTA实验04/Demo2.java)
	- [7-3 jmu-Java-03面向对象基础-03-形状](./20专升本-JAVA-PTA实验04/Demo3.java)
	- [7-4 jmu-Java-03面向对象基础-05-覆盖](./20专升本-JAVA-PTA实验04/Demo4.java)
	- [7-5 jmu-Java-03面向对象-06-继承覆盖综合练习-Person、Student、Employee、Company](./20专升本-JAVA-PTA实验04/Demo5.java)
- 实验05
	- [7-1 消失的车](./20专升本-JAVA-PTA实验05/Demo1.java)
	- [7-2 判断回文](./20专升本-JAVA-PTA实验05/Demo2.java)
	- [7-3 我是升旗手](./20专升本-JAVA-PTA实验05/Demo3.java)
- 实验06
	- [7-1 jmu-Java-04面向对象进阶-01-接口-Comparable](./20专升本-JAVA-PTA实验06/Demo1.java)
	- [7-2 jmu-Java-04面向对象进阶--02-接口-Comparator](./20专升本-JAVA-PTA实验06/Demo2.java)
	- [7-3 jmu-Java-06异常-01-常见异常](./20专升本-JAVA-PTA实验06/Demo3.java)
	- [7-4 jmu-Java-06异常-02-使用异常机制处理异常输入](./20专升本-JAVA-PTA实验06/Demo4.java)
	- [7-5 jmu-Java-06异常-03-throw与throws](./20专升本-JAVA-PTA实验06/Demo5.java)
	- [7-6 jmu-Java-06异常-04-自定义异常(综合)](./20专升本-JAVA-PTA实验06/Demo6.java)
- 天梯赛题集(1)
	- [7-1 N个数求和 (20分)](./天梯题集（1）/Pta1.java)
	- [7-2 比较大小 (10分)](./天梯题集（1）/Pta2.java)
	- [7-4 计算指数 (5分)](./天梯题集（1）/Pta3.java)
	- [7-5 计算阶乘和 (10分)](./天梯题集（1）/Pta4.java)
	- [7-7 简单题 (5分)](./天梯题集（1）/Pta5.java)
	- [7-7 跟奥巴马一起画方块 (15分)](./天梯题集（1）/Pta6.java)
	- [7-8 查验身份证 (15分)](./天梯题集（1）/Pta7.java)
	- [7-10 树的遍历 (25分)](./天梯题集（1）/Pta8.java)
	- [7-12 最长对称子串 (25分)](./天梯题集（1）/Pta9.java)
	


### 联系方式
QQ：1448801212<br></br>
Blog：[http://www.ls168.life:168](http://www.ls168.life:168)