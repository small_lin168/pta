import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Rectangle[] r_list = new Rectangle[2];
		Circle[] c_list = new Circle[2];
		for (int i = 0; i < 2; i++) {
			int width = sc.nextInt();
			int length = sc.nextInt();
			r_list[i] = new Rectangle(width, length);
		}
		for (int i = 0; i < 2; i++) {
			int radius = sc.nextInt();
			c_list[i] = new Circle(radius);
		}
		System.out.println(r_list[0].getPerimeter() + r_list[1].getPerimeter() + c_list[0].getPerimeter()
				+ c_list[1].getPerimeter());
		System.out.println(r_list[0].getArea() + r_list[1].getArea() + c_list[0].getArea() + c_list[1].getArea());
		// 通过遍历
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				System.out.println(r_list[i].toString()+"]");
				break;
			}
			System.out.print("["+r_list[i].toString() + ", ");
		}
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				System.out.println(c_list[i].toString()+"]");
				break;
			}
			System.out.print("["+c_list[i].toString() + ", ");
		}
	}
}

// 长方形
class Rectangle {
	private int width, length;

	public Rectangle(int width, int length) {
		this.width = width;
		this.length = length;
	}

	public int getPerimeter() {
		// 求周长
		return 2 * width + 2 * length;
	}

	public int getArea() {
		// 求面积
		return width * length;
	}

	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}

}

// 圆形
class Circle {
	private int radius;

	public Circle(int radius) {
		this.radius = radius;
	}

	public int getPerimeter() {
		// 求周长
		return (int) (Math.PI * 2 * radius);
	}

	public int getArea() {
		// 求面积
		return (int) (Math.PI * radius * radius);
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}

}
