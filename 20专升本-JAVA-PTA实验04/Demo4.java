import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n1 = sc.nextInt();
		int n2 = sc.nextInt();
		PersonOverride[] persons1 = new PersonOverride[n1];
		PersonOverride[] persons2 = new PersonOverride[n2];
		// 将默认的存放进去
		for (int i = 0; i < n1; i++) {
			persons1[i] = new PersonOverride();
		}
		for (int i = 0; i < n2; i++) {
			String name = sc.next();
			int age = sc.nextInt();
			boolean gender = sc.nextBoolean();
			PersonOverride temp = new PersonOverride(name, age, gender);
			boolean flag = true;
			for (int j = 0; j < n2; j++) {
				if (temp.equals(persons2[j])) {
					flag = false;
				}
			}
			if (flag) {
				persons2[i] = new PersonOverride(name, age, gender);
			}
		}
		for (int i = 0; i < n1; i++) {
			System.out.println(persons1[i]);
		}
		int i, count = 0;
		for (i = 0; i < n2; i++) {
			if (persons2[i] == null) {
				continue;
			}
			count++;
			System.out.println(persons2[i]);
		}
		System.out.println(count);
		System.out.println(Arrays.toString(PersonOverride.class.getConstructors()));
	}
}

class PersonOverride {
	private String name;
	private int age;
	private boolean gender;

	public PersonOverride() {
		this("default", 1, true);
	}
	
	public PersonOverride(String name, int age, boolean gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "" + name + "-" + age + "-" + gender + "";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + (gender ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonOverride other = (PersonOverride) obj;
//		boolean b1=Objects.equals(this.name, other.name);
		boolean b1 = (this.name.equals(other.name));
		boolean b2 = (this.age == other.age);
		boolean b3 = (this.gender == other.gender);
		if (b1 && b2 && b3) {
			return true;
		}
		return false;
	}

}