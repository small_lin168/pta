import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

abstract class Person {
	public String name;
	public int age;
	public boolean gender;

	public Person(String name, int age, boolean gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "" + name + "-" + age + "-" + gender + "";
	}

	public boolean equals(Person obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		boolean b1 = (this.name.equals(other.name));
		boolean b2 = (this.age == other.age);
		boolean b3 = (this.gender == other.gender);
		if (b1 && b2 && b3) {
			return true;
		}
		return false;
	}
}

class Student extends Person {
	private String stuNo;
	private String clazz;

	public Student(String name, int age, boolean gender, String stuNo, String clazz) {
		super(name, age, gender);
		this.stuNo = stuNo;
		this.clazz = clazz;
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Student:" + super.toString() + "-" + stuNo + "-" + clazz + "";
	}

	public boolean equals(Student obj) {
		if (super.equals(obj)) {
			return this.stuNo.equals(obj.stuNo) && this.clazz.equals(obj.clazz);
		} else {
			return false;
		}
	}
}

class Company {
	public String name;

	public Company(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}

	public boolean equals(Company obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (this.name.equals(other.name)) {
			return true;
		}
		return false;
	}
}

class Employee extends Person {
	private double salary;
	public Company company;

	public Employee(String name, int age, boolean gender, double salary, Company company) {
		super(name, age, gender);
		this.salary = salary;
		this.company = new Company(company.name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Employee:" + super.toString() + "-" + company + "-" + salary + "";
	}

	public boolean equals(Employee obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		DecimalFormat df = new DecimalFormat("#.#");
		if (super.equals(obj)) {
			return df.format(obj.salary).equals(df.format(this.salary)) && this.company.equals(obj.company);
		} else {
			return false;
		}
	}

}

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		List<Person> personList = new ArrayList<Person>();
		List<Student> stuList = new ArrayList<Student>();
		List<Employee> empList = new ArrayList<Employee>();

		while (true) {
			String perInfo = sc.nextLine();
			String info[] = perInfo.split(" ");

			if (info[0].equals("s")) {
				Student s = new Student(info[1], 
						Integer.parseInt(info[2]), 
						Boolean.parseBoolean(info[3]), 
						info[4],
						info[5]);
				personList.add(s);
			} else if (info[0].equals("e")) {
				Employee e = new Employee(info[1], 
						Integer.parseInt(info[2]), 
						Boolean.parseBoolean(info[3]),
						Double.parseDouble(info[4]), 
						new Company(info[5]));
				personList.add(e);
			} else
				break;
			//利用Collections进行类元素的比较
			Collections.sort(personList, new Comparator<Person>() {
				@Override
				public int compare(Person o1, Person o2) {
					if (o1.equals(null) && o2.equals(null))
						return 0;
					if (o1.name.compareTo(o2.name) == 0)
						return o1.age - o2.age;
					else
						return o1.name.compareTo(o2.name);
				}
			});
		}
		for (Person p : personList)
			System.out.println(p.toString());
		if (sc.next().equals("exit")) {
			return;
		}
		// 对象分类
		for (Person p : personList) {
			boolean flag = true;
			if (p instanceof Student) {
				// 比较相同对象
				for (Student s : stuList) {
					if (s.equals((Student) p)) {
						flag = false;
						break;
					}
				}
				if (flag)
					stuList.add((Student) p);
			} else if (p instanceof Employee) {
				for (Employee e : empList) {
					if (e.equals((Employee) p)) {
						flag = false;
						break;
					}
				}
				if (flag)
					empList.add((Employee) p);
			}
		}
		// 输出对象
		System.out.println("stuList");
		for (Student s : stuList)
			System.out.println(s.toString());
		System.out.println("empList");
		for (Employee e : empList)
			System.out.println(e.toString());
	}
}