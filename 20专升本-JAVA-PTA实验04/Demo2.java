import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		String[][] arr_1 = new String[n][3];
		Person[] person_list = new Person[n];
		for (int i = 0; i < n; i++) {
			String name = sc.next();
			int age = sc.nextInt();
			boolean gender = sc.nextBoolean();
			arr_1[i][0] = name;
			arr_1[i][1] = String.valueOf(age);
			arr_1[i][2] = String.valueOf(gender);
		}
		// 倒序输出
		for (int i = 0; i < n; i++) {
				person_list[i] = new Person(arr_1[i][0], Integer.parseInt(arr_1[i][1]), Boolean.parseBoolean(arr_1[i][2]));
		}
		for (int i = n-1; i >= 0; i--) {
			System.out.println(person_list[i].toString());
		}
		System.out.println(new Person().toString());
	}
}

class Person {
	private String name;
	private int age;
	private boolean gender;
	private int id;
	private static int count;

	static {
		System.out.println("This is static initialization block");
	}

	{
		id=count++;
		System.out.println("This is initialization block, id is " + id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// 无参
	public Person() {
		System.out.println("This is constructor");
		System.out.println(name + "," + age + "," + gender + "," + id);
	}

	// 有参
	public Person(String name, int age, boolean gender) {
		this.name = name;
		this.gender = gender;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
	}
}
