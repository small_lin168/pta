package pta6;

import java.util.Scanner;

class ArrayUtils {
	public static double findMax(double[] arr, int begin, int end) throws IllegalArgumentException {
		if(begin < 0){
			throw new IllegalArgumentException("begin:" + begin + " < 0");
		}
		else if(begin >= end){
			throw new IllegalArgumentException("begin:" + begin + " >= end:" + end);
		}
		else if(end > arr.length){
			throw new IllegalArgumentException("end:" + end + " > arr.length");
		}
		double max = arr[begin];
		for(int i = begin; i < end; i++) {
			if(arr[i] > max) max = arr[i];
		}
		return max;
	}
}

public class Demo5 {
	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		int n=sc.nextInt();
		double[] i_arr=new double[n];
		for(int i=0;i<n;i++){
			i_arr[i]=sc.nextInt();
		}
		boolean flag=true;
		while(flag) {
			try {
				int n1 = sc.nextInt();
				int n2 = sc.nextInt();
				System.out.println(ArrayUtils.findMax(i_arr, n1, n2));
			}
			catch (IllegalArgumentException e) {
				System.out.println(e);
			}
			catch (Exception e) {
				flag = false;
			}
		}
		sc.close();
		
		try {
		     System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class,int.class,int.class));
		} catch (Exception e1) {
			
		} 
	}
}
