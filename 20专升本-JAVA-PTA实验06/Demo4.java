package pta6;

import java.util.Scanner;
import java.util.Arrays;

public class Demo4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] num_arr = new int[n];
		for(int i = 0; i < n; i++) {
			try {
				num_arr[i] = Integer.parseInt(sc.next());
			} catch (Exception e) {
				System.out.println(e);
				i--;
			}
		}
		System.out.println(Arrays.toString(num_arr));
	}

}
