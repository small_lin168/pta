package pta6;

import java.util.Scanner;

public class Demo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] i_arr=new int[5];
		Scanner sc=new Scanner(System.in);
		boolean flag=true;
		while(flag){
			String[] jump=sc.nextLine().split(" ");
			try {
				switch(jump[0]){
					case "arr":
						//使用这个数组的这个元素
						i_arr[Integer.parseInt(jump[1])] = 0;
						break;
					case "null":
						throw new NullPointerException();
					case "cast":
						@SuppressWarnings("unused")
						Integer tmp = (Integer)((Object) new String(""));
						break;
					case "num":
						Integer.parseInt(jump[1]);
						break;
					default:
						flag=false;
						break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

}
