package pta6;

import java.util.Arrays;
import java.util.Scanner;

class PersonSortable implements Comparable<PersonSortable> {

	private String name;
	private int age;

	public PersonSortable(String name, int age) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.age = age;
	}

	@Override
	public int compareTo(PersonSortable o) {
		// TODO Auto-generated method stub
		//判断是否名字相同
		if (this.name.equals(o.name))
			//按年龄升序
			return this.age - o.age;
		return this.name.compareTo(o.name);
	}

	@Override
	public String toString() {
		return name + "-" + age;
	}

}

public class Demo1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n = Integer.parseInt(sc.nextLine());
		PersonSortable[] p_list = new PersonSortable[n];
		for (int i = 0; i < n; i++) {
			String[] p_data = sc.nextLine().split(" ");
			p_list[i] = new PersonSortable(p_data[0], Integer.parseInt(p_data[1]));
		}
		Arrays.sort(p_list);
		for (PersonSortable p : p_list) {
			System.out.println(p);
		}
		System.out.println(Arrays.toString(PersonSortable.class.getInterfaces()));

	}

}
