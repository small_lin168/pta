package pta6;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

class PersonSortable2{
	private String name;
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public PersonSortable2(String name, int age) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return name + "-" + age;
	}
}

//按照名字进行排序
class NameComparator implements Comparator<PersonSortable2> {
	public int compare(PersonSortable2 o1, PersonSortable2 o2) {
		return o1.getName().compareTo(o2.getName());
	}
}
//按照年龄进行排序
class AgeComparator implements Comparator<PersonSortable2> {
	public int compare(PersonSortable2 o1, PersonSortable2 o2) {
		return o1.getAge() - o2.getAge();
	}
}

public class Demo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n = Integer.parseInt(sc.nextLine());
		PersonSortable2[] pList = new PersonSortable2[n];
		
		for(int i = 0; i < n; i++) {
			String[] pData = sc.nextLine().split(" ");
			pList[i] = new PersonSortable2(pData[0], Integer.parseInt(pData[1]));
		}
		
		//按照名字排序
		Arrays.sort(pList, new NameComparator());
		System.out.println("NameComparator:sort");
		for(PersonSortable2 p : pList){
			System.out.println(p);
		}
		//按照年龄排序
		Arrays.sort(pList, new AgeComparator());
		System.out.println("AgeComparator:sort");
		for(PersonSortable2 p : pList){
			System.out.println(p);
		}
		System.out.println(Arrays.toString(NameComparator.class.getInterfaces()));
		System.out.println(Arrays.toString(AgeComparator.class.getInterfaces()));
	}

}
